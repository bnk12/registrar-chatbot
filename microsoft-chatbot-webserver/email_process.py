#send_email() sends an email to the duke registrar with the users unanswered question 
#and email address

#@param JSON object 

import sys
import smtplib
from socket import gaierror 

def send_email(dict): 
    #parsing JSON object for question 
    question = dict["question"]
    
    port = 587
    smtp_server = "smtp.office365.com"
    login = "registrarbot@outlook.com" 
    password = "Chat-bot"
    
    # the sender’s and receiver’s email addresses
    sender = dict["sender"]
    #sender = "registrarbot@outlook.com"
    receiver = "jkc45@duke.edu"

    # type your message: use two newlines (\n) to separate the subject from the message body, and use 'f' to  automatically insert variables in the text
    message = f"""\
    Subject: Hi from IBM
    To: {receiver}
    From: {sender}

    This is my first message with Python.
    My question was {question}
    """


    try:
        #send your message with credentials specified above
        with smtplib.SMTP(smtp_server, port) as server:
            server.starttls()
            server.login(login, password)
            server.sendmail(sender, receiver, message)

        # tell the script to report if your message was sent or which errors need to be fixed
        print('Sent')
    except (gaierror, ConnectionRefusedError):
        print('Failed to connect to the server. Bad connection settings?')
    except smtplib.SMTPServerDisconnected:
        print('Failed to connect to the server. Wrong user/password?')
    except smtplib.SMTPException as e:
        print('SMTP error occurred: ' + str(e))
    
    
    return { 'message': 'Hello world' }    


 

