CREATE DATABASE docker_test_db;
use docker_test_db;

CREATE TABLE test_table (
  question VARCHAR(1024),
  sender VARCHAR(1024)
);

INSERT INTO test_table
  (question, sender)
VALUES
  ('How do I request a transcript?', 'registrarbot@outlook.com');